# vue-setup

This is a try for defining an alternative vue layout which is itended to be more strightforward in a def ways:

+ decoupling different application layers
+ use less vue-specific suntax in favour to standard es mechanisms
+ become more friendly to react guys :)

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev
```