export default view => ({
  name: 'app',
  props: {
    label: {
      type: String,
      default: 'label'
    },
    data: {
      type: String,
      required: true
    }
  },
  render(h) {
    return view(h)(
        this.label,
        parseInt(this.data),
        msg => alert(msg)
    )
  }
})