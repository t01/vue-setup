export default h => (x, v, onClick) => (
    <p onClick={() => onClick(`clicked var is ${x}`)}>
    {v === 1 && <b>first: {x}</b>}
    {v === 2 && <b>second: {x}</b>}
    {v === 3 && <b>third: {x}</b>}
    {v > 3 && <b>any other: {x}</b>}
    </p>
)